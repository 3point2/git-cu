# git-cu 👋

git-cu helps keep your local git repositories organized by cloning them into a
directory structure based on their URL.

For example, running a regular "git clone" of this repository with `git clone
https://gitlab.com/3point2/git-cu.git` will create a `git-cu` directory inside
your current working directory. Using "git cu" instead of "git clone" will
create a `gitlab.com/3point2/git-cu` directory structure inside your current
working directory.

cu is short for "clone URL", pronounced "see you".

## Quick Start

Install git-cu with `pip install git-cu`.

You can then clone repositories with `git cu <url>`. Any options given
**after** the URL are passed directly to git-clone.

If the environment variable GIT_CU_DIR is set, git-cu will prefix the
destination directory with its value.

## Examples

This is what cloning this repository with git-cu looks like:
```console
$ git cu https://gitlab.com/3point2/git-cu.git
Cloning into 'gitlab.com/3point2/git-cu'...
remote: Enumerating objects: 39, done.
remote: Counting objects: 100% (39/39), done.
remote: Compressing objects: 100% (37/37), done.
remote: Total 39 (delta 15), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (39/39), 28.66 KiB | 1.15 MiB/s, done.
```

The resulting directory structure looks like this:

```console
$ tree -d -L 3
.
└── gitlab.com
    └── 3point2
        └── git-cu

$ ls gitlab.com/3point2/git-cu/
gitcu  LICENSE  poetry.lock  pylintrc  pyproject.toml  README.md
```

Exporting the environment variable GIT_CU_DIR in your .bashrc (or equivalent
for other shells) allows you to conveniently run "git cu" commands from any
directory without having to change into your preferred destination directory
first:

```console
$ export GIT_CU_DIR=~/code

$ cd /usr/local/bin/

$ ls  # the current directory is empty

$ git cu https://gitlab.com/3point2/git-cu.git
GIT_CU_DIR is /home/user/code
Cloning into '/home/user/code/gitlab.com/3point2/git-cu'...
remote: Enumerating objects: 39, done.
remote: Counting objects: 100% (39/39), done.
remote: Compressing objects: 100% (37/37), done.
remote: Total 39 (delta 15), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (39/39), 28.66 KiB | 1.10 MiB/s, done.

$ ls  # the current directory is still empty

$ ls ~/code/gitlab.com/3point2/  # the clone was done in ~/code
git-cu
```

## Installation

git-cu requires Python 3.6 or later.

git-cu is available on [PyPi](https://pypi.org/project/git-cu/) and can be
installed with pip.

    pip install git-cu

## Usage

git-cu passes all options specified **after** the URL to git-clone. This
example passes the "--bare" option to "git-clone":

    git cu https://gitlab.com/3point2/git-cu.git --bare

For a summary of options accepted by git-cu itself, run `git cu -h`.
